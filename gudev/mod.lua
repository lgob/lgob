MODULE = 'gudev'
init()

mod = {
    name = 'gudev',
    pkg  = 'gudev-1.0',
}

gen_iface(mod)
compile  (mod)
install  (mod, LIB)
clean    (mod)
