#! /usr/bin/env lua

require "lgob.gtk"
require "lgob.gudev"

-- do not listen to any uevents
-- client = gudev.Client.new(nil)
-- listen to uevents on all subsystems
client = gudev.Client.new({})
-- listen only to uevents on the "block" subystem
-- client = gudev.Client.new({"block"})

-- list all block devices
devs = client:query_by_subsystem("block")
for _, dev in ipairs(devs) do
	print("dev: ", dev, dev:get_name())
end

enum = gudev.Enumerator.new(client)

-- search the sda swap partition(s)
enum:add_match_subsystem("block")
enum:add_match_name("sda*")
enum:add_match_property("ID_FS_TYPE", "swap")

devs = enum:execute()

for _, dev in ipairs(devs) do
	print("sda swap dev: ", dev, dev:get_name())
end

function on_uevent(client, action, dev, data)
	print("on_uevent", action, dev, dev:get_subsystem(), dev:get_name())
end

client:connect("uevent", on_uevent, client)

window = gtk.Window.new()
window:show_all()
window:connect("delete-event", gtk.main_quit)

gtk.main()

